<?php
include('header.html');
?>
<!doctype html>
<html lang="en-us">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
<title>Home Page</title>

    <script src="scripts/script.js"></script>
    <script src="scripts/api_data.js"></script>
    <script src="scripts/sprintf.js"></script>
    <script src="scripts/general.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?c&key=AIzaSyAN_3opn1R_j0HDbx2J87331qbQLolUVQw"></script>
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script>
    	//This function sets the functions to be called when the website loaded
        function initialize(){
            getLocation();
            xmlRequest(getTransports(), getTransportsCallback);
            jsFunction();
            initMap(lat,lng);
            timetable();
            nextDepartures();
        }
    </script>
    <script type="text/javascript" src="scripts/2.0.0-crypto-sha1.js"></script>
    <script type="text/javascript" src="scripts/2.0.0-hmac-min.js"></script>
    <link href="style/style.css" rel="stylesheet" type="text/css">
	  <meta charset="utf-8">
    
</head>
<body onload="initialize();">
<div class="container">
 <!-- This section displays the buses per stop departing in next 20 minutes-->
<div class = "row">
<h2>Buses & Trams departing next 20 mins from Latrobe famous stops </h2>
<br/>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<table class="table table-condensed" id="depart"></table>

</div>
</div>
<div class="clear"></div>
 <!-- This section we can select the bus stops-->
<div class = "row">
<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
	
      <h3>Search Buses & Trams </h3>
    <form role = "form">
    <div class = "form-group">
	<label>Campus: </label>
    <select name="Campus" class = "form-control">
    <option value="Bundoora">Bundoora</option>
    </select>
	</div>
    
    <div class  ="form-group">
          <label>Stop:</label>
          <select onchange="jsFunction()" id="stops" class = "form-control">
          </select>
    </div>

    <div>
        <a href="#map" style="color: inherit; display: inline-block"><button type="button" class="btn btn-info btn-md" onclick="navigate(lat,lng)">Navigate</button></a>
        <a href="#map" style="color: inherit; display: inline-block"><button onclick="xmlRequest(stopsNearby(myLat,myLon),stopsNearbyCallback)" type="button" class="btn btn-info btn-md" >Stop Nearby</button></a>
    </div>
	</form>
</div>

<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
<form role = "form">
<h3>Search Bus TimeTable </h3>


    <div class = "form-group">
    <label>Bus Number:</label>
        <select name="busNumber" id="busNumber" onchange="timetableDropdown()" class = "form-control"></select>
    </div> 
    <div class = "form-group">
    <label>Going Towards:</label>
        <select name="route" id="goingTowards" class ="form-control"></select>
    </div>
    <div>
        <button type="button" class="btn btn-info btn-md" onclick="showTimetable();">View Timetable</button>
       <button type="button" class="btn btn-info btn-md"onclick="viewBusRoute()">View Bus Route</button>

    </div>

</form>
</div>
</div>

<br/>
<!--This section display a map where it shows the bus stop-->
    <a name="map"></a>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="map">
    <div id="floating-panel" style="text-align: left; color: black">
        <h4>STOPS AROUND YOUR CAMPUS:</h4>
    </div>
<div id="map"></div>
</div>
</div>
</body>
</html>
<?php
//include('footer.html');
?>