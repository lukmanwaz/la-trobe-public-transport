function initMap(lat,lng) //This function creates a map to be displayed
{
    var mapProp = {
        center:new google.maps.LatLng(lat,lng),
        zoom:18,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("map"),mapProp);

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat,lng),
        map: map,
        title: 'Hello World!'
    });
}

function jsFunction() //This function gets the selected text from the stops drop down list and display the location on the map
{
    var myselect = document.getElementById("stops"); //It selects the stops dropdown item
    var selectedOption = myselect.options[myselect.selectedIndex]; //This gets the selected option from the dropdown
    var selectedText = selectedOption.text; //This gets the text of the selected item

    switch (selectedText) {
        case "Ablett Tower/Moat Dr":
            lat = geoj[0].geometry.coordinates[0];
            lng = geoj[0].geometry.coordinates[1];
            initMap(lat,lng);
            break;
        case "La Trobe Uni Thomas Cherry Building/Science Dr":
            lat = geoj[1].geometry.coordinates[0];
            lng = geoj[1].geometry.coordinates[1];
            initMap(lat,lng);
            break;
        case "Kingsbury Dr/Waterdale Rd":
            lat = geoj[2].geometry.coordinates[0];
            lng = geoj[2].geometry.coordinates[1];
            initMap(lat,lng);
            break;
        case "La Trobe Uni Terminus":
            lat = geoj[3].geometry.coordinates[0];
            lng = geoj[3].geometry.coordinates[1];
            initMap(lat,lng);
            break;
        case "La Trobe University/Plenty Rd #60":
            lat = geoj[4].geometry.coordinates[0];
            lng = geoj[4].geometry.coordinates[1];
            initMap(lat,lng);
            break;
        default:
            initMap(lat,lng);

    }


}
 function navigate(navLat, navLng) // this function will navigate the user from his/her location to the stop he/she have selected
 {
     initMap();

     function initMap() {
         var directionsService = new google.maps.DirectionsService;
         var directionsDisplay = new google.maps.DirectionsRenderer;
         var map = new google.maps.Map(document.getElementById('map'), {
             zoom: 7,
             center: {lat: -37.72219, lng: 145.048477}
         });
         directionsDisplay.setMap(map);

         calculateAndDisplayRoute(directionsService, directionsDisplay);

     }

     //This function creates a route form the user's location to the stop selected
     function calculateAndDisplayRoute(directionsService, directionsDisplay)
     {
         directionsService.route({
             origin: new google.maps.LatLng(myLat,myLon),
             destination: new google.maps.LatLng(navLat,navLng),
             travelMode: google.maps.TravelMode.WALKING
         }, function(response, status) {
             if (status === google.maps.DirectionsStatus.OK) {
                 directionsDisplay.setDirections(response);
             } else {
                 window.alert('Directions request failed due to ' + status);
             }
         });
     }
 }

//This function gets the next departures from ptv and display the next bus departing from each stop
function nextDepartures()
{
    var modeId; //It stores the modeId such as whether it is bus,train,tram
    var stopId; //It stores the stopId of that particular stop

    //This is the initial layout of the table for showing timetable
   var s = "<tr class='warning'>" +
        "<td style='font-weight: bold; text-align: left;'>Time</td>" +
        "<td style='font-weight: bold; text-align: left;'>Stop</td>" +
        "<td style='font-weight: bold; text-align: left;'>Route</td>" +
        "<td style='font-weight: bold; text-align: left;'>Destination</td>" +
        "</tr>";
    var limit = 1; //It sets the limit the ptv should return, if its 0 it is for the whole day

    for (var i = 0; i < geoj.length; i++) {

        modeId = geoj[i].properties.route_type;
        stopId = geoj[i].properties.stop_id;

        xmlRequest(broadNextDeparturesURL(modeId, stopId, limit), broadNextDeparturesCallback);
    }

    var nowDate = new Date(); //Current date and time
    nowDate.setMinutes(nowDate.getMinutes()+20);

    //It sorts the buses according to the time
    broadjson.sort(function (a, b) {
        return a.values.time_timetable_utc - b.values.time_timetable_utc;
    });

    for(i =1; i < broadjson.length;i++)
    {
        var date = new Date(broadjson[i].values[0].time_timetable_utc); //It gets the time of the particular bus
        var minutes; //It stores the minutes of the particular bus
        if(date < nowDate)
        {
            var time = formatAMPM(date);

            var location;
            switch (broadjson[i].values[0].platform.stop.location_name)
            {
                case "La Trobe Uni Thomas Cherry Building/Science Dr":
                    location = "Thomas Cherry Building";
                    break;

                case "La Trobe University/Plenty Rd #60":
                    location = "Plenty Road Tram #60";
                    break;

                default:
                    location = broadjson[i].values[0].platform.stop.location_name;
            }
            //This display the data in a table
            s += "<tr class='active'>" +
        "<td style='width: auto'>" + time + "</td>" +
        "<td>" + location + "</td>" +
        "<td>" + broadjson[i].values[0].platform.direction.line.line_name + "</td>" +
        "<td>" + broadjson[i].values[0].run.destination_name + "</td>" +
        "</tr>";

        }
    }
    document.getElementById("depart").innerHTML =s;
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes ;
    return strTime;
}

//It gets the timetable for a particular bus
function timetable()
{
    var modeId; //It stores the modeId such as whether it is bus,train,tram
    var stopId; //It stores the stopId of that particular stop
    var s = []; //It stores the line id of that particular stop
    var line_ids = []; //It stores the line id of all stops

    var limit = 0; //It sets the limit the ptv should return, if its 0 it is for the whole day

    modeId = geoj[1].properties.route_type;
    stopId = geoj[1].properties.stop_id;

    xmlRequest(broadNextDeparturesURL(modeId, stopId, limit),broadNextDeparturesCallback);

    for(i = 0; i < broadjson[0].values.length; i++)
    {
        //It gets the line id from the array and store it in a variable
        var line_id = broadjson[0].values[i].platform.direction.line.line_id;
        if(s.indexOf(line_id) == -1 ) {
            s.push(line_id);
            //It gets the line name and store it in a variable
            var line_name = broadjson[0].values[i].platform.direction.line.line_name;
            //creates an array of line id and line name
            var z = [line_id, line_name];
            line_ids.push(z);
        }
    }

    var x = document.getElementById("busNumber"); //It selects the busnumber dropdown item

    for (i = 0; i < line_ids.length; i++) {
        var option = document.createElement("option"); //Creates an option in the busnumber dropdown
        option.text = line_ids[i][1];
        x.add(option); //Adds busnumbers as options
    }

    timetableDropdown();

}

//This function get the bus number and create a drop down list of which directions the bus goes.
function timetableDropdown()
{
    var y = document.getElementById("goingTowards"); //It selects the goingtowards dropdown item
    removeOptions(y);

    var myselect = document.getElementById("busNumber"); //It selects the busnumber dropdown item
    var selectedOption = myselect.options[myselect.selectedIndex]; //This gets the selected option from the dropdown
    var busNumber = selectedOption.text; //This gets the text of the selected item

    var dir_ids = []; //It stores the direction names

    for (i = 0; i < broadjson[0].values.length; i++) {
        if (busNumber == broadjson[0].values[i].platform.direction.line.line_name) {
            //Gets the direction name from the next departures array
            var dir_id = broadjson[0].values[i].platform.direction.direction_name;
            if(dir_ids.indexOf(dir_id) == -1)
            {
                dir_ids.push(dir_id); //It stores the direction names in an array
            }
        }
    }

    for (i = 0; i < dir_ids.length; i++) {
        var option = document.createElement("option"); //It creates a option in the dropdown list
        option.text = dir_ids[i];
        y.add(option);
    }
}

//This function gets the bus number and direction and displays the timetable for that particular bus
function showTimetable()
{
    //This is the initial layout of the table for showing timetable
    var s = "<tr class='warning' style='font-size:1.5em;'>" +
            "<td width='10%'>Time</td>" +
            "<td>Route</td>" +
            "<td>Destination</td>" +
            "</tr>";

    var x = document.getElementById("busNumber"); //It selects the busnumber dropdown item
    var selectedOption1 = x.options[x.selectedIndex]; //This gets the selected option from the dropdown
    var busNumber = selectedOption1.text; //This gets the text of the selected item

    var y = document.getElementById("goingTowards"); //It selects the goingtowards dropdown item
    var selectedOption2 = y.options[y.selectedIndex]; //This gets the selected option from the dropdown
    var goingTowards = selectedOption2.text; //This gets the text of the selected item

    for (i = 0; i < broadjson[0].values.length; i++) {
        // This gets the busnumber from the array we create by getting ptv data
        var xBusNumber = broadjson[0].values[i].platform.direction.line.line_name;

        // This gets the direction name from the array we create by getting ptv data
        var xGoingTowards = broadjson[0].values[i].platform.direction.direction_name;

        //check whether the selected option of busnumber and direction name are same as in the array
        if(busNumber == xBusNumber && goingTowards == xGoingTowards)
        {
            var nowDate = new Date(); //Current date and time
            var date = new Date(broadjson[0].values[i].time_timetable_utc); //The date and time of the particular bus
            var minutes = date.getMinutes(); //minutes of that particular bus
            if(date.getMinutes() < 10)
            {
                minutes = "0" + date.getMinutes();
            }
            else
            {
                minutes = date.getMinutes();
            }

            var maximumHours = new Date();
            maximumHours.setHours(nowDate.getHours() + 3);

            //display only the timetables of the buses departing after the current time and within 3 hours
            if(date > nowDate && date < maximumHours)
            {
                s += "<tr class='active' style=' font-weight:bold'>" +
                    "<td>" + date.getHours() + ":" + minutes + "</td>" +
                    "<td>" + broadjson[0].values[i].platform.direction.line.line_name + "</td>" +
                    "<td>" + broadjson[0].values[i].run.destination_name + "</td>" +
                    "</tr>";

            }
        }
    }


    localStorage.setItem("timetable",s); //Saves the timetable data on local storage so we can display it in timetable page
    if(s.length >109) {
        location.href = 'timetable.php';
    }
    else
    {
        window.alert("No more buses Today!");
    }

}
// This function empties the array
function removeOptions(obj) {
    if (obj == null) return;
    if (obj.options == null) return;
    obj.options.length = 0;
}

function viewBusRoute()
{
    display();
    //This function gets the bus number and creates a url for the pdf file of the bus route
    function print(bus){
        var busRoute = ""; // It used to store the bus route number accoring to the parameter passed

        switch(bus) {
            case "250":
                busRoute = "250";
                break;

            case "301":
                busRoute = "301";
                break;

            case "350":
                busRoute = "350";
                break;

            case "400":
                busRoute = "400+401";
                break;

            case "401":
                busRoute = "400+401";
                break;

            case "548":
                busRoute = "548";
                break;

            case "550":
                busRoute = "550";
                break;


            case "551":
                busRoute = "551";
                break;

            case "561":
                busRoute = "561";
                break;

            case "562":
                busRoute = "562";
                break;

            case "566":
                busRoute = "566";
                break;

            case "61":
                busRoute = "61";
                break;

            case "62":
                busRoute = "62";
                break;

            case "63":
                busRoute = "63";
                break;

            case "Route 1":
                busRoute = "Route1";
                break;

            case "Route 2":
                busRoute = "Route2";
                break;

            case "Route 3":
                busRoute = "Route3";
                break;

            case "Route 7":
                busRoute = "Route7";
                break;

            case "Route O":
                busRoute = "RouteO";
                break;


        }
        var URL = "busRoute/"; // This is the url of the folder of bus route pdfs
        URL = URL + busRoute + ".pdf";
        return URL;
    }

    //This function displays the bus route pdf or downloads it
    function display(){
        var x = document.getElementById("busNumber"); //It selects the busnumber dropdown item
        var selectedOption1 = x.options[x.selectedIndex]; //It gets the selected option
        var res = selectedOption1.text; //It gets the text of the selected item
        var busNumber = ""; //It used to store the url of the bus returned from print function

        for(x=0; x <res.length; x++)
        {
            busNumber = res.substring(0, 3);
        }

        location.href = print(busNumber);
    }
}