var baseURL = "http://timetableapi.ptv.vic.gov.au"; //The url for ptv api
var timestamp = new Date().toJSON().toString(); //Current Time
var finalURL; //url returned to call the ptv api
var stopsNearbyJSON; // Nearby Stops are stored
var lat; //The latitude of the stop changes according to the selected stop
var lng; //The longitude of the stop changes according to the selected stop
var geoj =[]; //The stops around the campus are to be stored
var broadjson = []; // The next departures from a bus stop are to be stored
var nextDepart =[]; // The specific departures from a bus stop, bus, and a specific route are to be stored
var myLat; // This stores the user's latitude
var myLon; // This stored the user's longitude

function getLocation() //It gets the user's location
 {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(userposition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function userposition(pos)  // This stores the user's location in a variable
{
    myLat = pos.coords.latitude;
    myLon = pos.coords.longitude;
}

function xmlRequest(finalURL, callback) //This gives the ptv url and get data from ptv
{
    var xhr = new XMLHttpRequest();
    xhr.open("GET", finalURL, false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            callback(xhr.responseText);
        }
    };
    xhr.send(null);
}

function calculateDistance(lon1,lat1,lon2, lat2)  //It calaculates the distance from a location to another
{
    //Reference: http://www.movable-type.co.uk/scripts/latlong.html

    var R = 3958.7558657440545; // Radius of earth in Miles
    var dLat = toRad(lat2-lat1);
    var dLon = toRad(lon2-lon1);
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;

}

function toRad(Value) {
    /** Converts numeric degrees to radians */
    //Reference: http://www.movable-type.co.uk/scripts/latlong.html

    return Value * Math.PI / 180;
}