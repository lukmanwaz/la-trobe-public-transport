//Generate Developer Key into cryto
function generateURLWithDevIDAndKey(base)
{
    var key = "ed279a12-ef03-11e5-a65e-029db85e733b";
    var id = "1000708";
    var endpoint = base + "devid=" + id;

    // hash the endpoint URL
    var signature = Crypto.HMAC(Crypto.SHA1, endpoint, key, false);

    // add API endpoint, base URL and signature together
    return baseURL + endpoint + "&signature=" + signature;
}

//Functions returns the URLs to connect to PTV API
function healthcheck() //This function returns a healthcheck url of the ptv server whether it is online
{
    var healthcheck = "/v2/healthcheck?timestamp=" + timestamp;

    if (healthcheck.indexOf("?") > 0)
    {
        healthcheck += "&";
    }
    else
    {
        healthcheck += "?";
    }

    finalURL = generateURLWithDevIDAndKey(healthcheck);

    return finalURL;
}

function stopsNearby(lat,lng) //It returns the stops nearby url to get from ptv api
{
    var stopsNearby = "/v2/nearme/latitude/";
    stopsNearby += lat;

    stopsNearby += "/longitude/";

    stopsNearby += lng + "?";
    finalURL = generateURLWithDevIDAndKey(stopsNearby);

    return finalURL;
}

function getTransports() //It returns the stops around url to get from ptv api
{
    var poi = "1,2"; // This defines the bus and tram
    var lat1 = -37.716333; // This is the latitude 1 of the campus
    var lng1 = 145.042843; // This is the latitude 2 of the campus
    var lat2 = -37.728288; // This is the longitude 1 of the campus
    var lng2 = 145.055104; // This is the longitude 2 of the campus
    var griddepth = 0;
    var limit = 10; //The limit of data to be returned from ptv

    var url = vsprintf("/v2/poi/%s/lat1/%s/long1/%s/lat2/%s/long2/%s/griddepth/%s/limit/%s?",[poi,lat1,lng1,lat2,lng2,griddepth,limit]);

    finalURL = generateURLWithDevIDAndKey(url);
    return finalURL;

}

function broadNextDeparturesURL(modeID, stopID, limit)  //It returns the next departures url to get from ptv api
{
    var url = vsprintf("/v2/mode/%s/stop/%s/departures/by-destination/limit/%s?", [modeID,stopID,limit]);
    finalURL = generateURLWithDevIDAndKey(url);

    return finalURL;
}

function specificNextDepartures(mode,line,stop,directionId) //It returns the specific next departures url to get from ptv api
{
    var limit = 3;

    var base = vsprintf("/v2/mode/%s/line/%s/stop/%s/directionid/%s/departures/all/limit/%s", [mode,line,stop,directionId,limit]);

    base += sprintf("?for_utc=%s&",timestamp);

    finalURL = generateURLWithDevIDAndKey(base);

    return finalURL;
}

//Callback Functions for the PTV data
function healthCheckCallback(data) //This function checks if the ptv server is working properly
{
    healthCheckStatus = true;
    var healthJSON = JSON.parse(data);

    // If any of the health check JSON members are false the health is not ok, i.e. false
    for(var member in healthJSON) {
        if(healthJSON[member]===false) {
            console.log(member + ": " + healthJSON[member]);
            healthCheckStatus = false;
        }
    }
    //console.log("Health Check: " + healthCheckStatus);

    // Handle the health check status
    if(healthCheckStatus) {
        document.write("Health check passed.");
    } else {
        document.write("Health check failed.");
    }
}


function stopsNearbyCallback(data) // This function get the stops nearby and calculate the nearest stop from the user
{
    stopsNearbyJSON = JSON.parse(data);

    var array = []; //This array stores the stops nearby data to be sorted

    for(i = 0; i < stopsNearbyJSON.length; i++) {
        var lon2 =stopsNearbyJSON[i].result.lon;
        var lat2 =stopsNearbyJSON[i].result.lat;
        var res = stopsNearbyJSON[i];
        array.push({
            result: res,
            distance: calculateDistance(myLon,myLat,lon2,lat2)
        });
    }

    // finaly, sort the cities according to their distance
    array.sort(function (a, b) {
        return a.distance - b.distance;
    });

    var lat = array[0].result.result.lat;
    var lng = array[0].result.result.lon;
    //initMap(lat,lng);

    navigate(lat,lng);
}

function getTransportsCallback(data) //This function get the stops nearby and store it in an array called geoj
{
    var transports = JSON.parse(data); //This gets the data returned from ptv

    for (var i = 0; i < transports.totalLocations; i++)
    {
        geoj.push({"geometry":{
            "type": "Point",
            "coordinates": [transports.locations[i].lat, transports.locations[i].lon]},
            "properties":{
                "transport_type": transports.locations[i].transport_type,
                "route_type": transports.locations[i].route_type,
                "stop_id": transports.locations[i].stop_id,
                "location_name": transports.locations[i].location_name}
        });
    }
    //This element take all the stops and display it in the dropdown list of the homepage
    for (i = 0; i < transports.totalLocations; i++) {
        var x = document.getElementById("stops");
        var option = document.createElement("option");
        option.text = transports.locations[i].location_name;
        x.add(option);
    }
}


function broadNextDeparturesCallback(data) //This function gets the next departures data and store it in a array broadjson
{
    var bnd = JSON.parse(data); //This gets the data returned from ptv

    broadjson.push(bnd);

}

function specificNextDeparturesCallback(data) //This function gets the next departures data and store it in a array nextDepart
{
    var snd = JSON.parse(data); //This gets the data returned from ptv

    nextDepart.push(snd);
}