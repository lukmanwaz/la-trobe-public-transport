
<?php
include('header.html');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Shibboleth Identity Provider - La Trobe University</title>
<meta name="viewport" content="initial-scale=1, minimum-scale=1">
<link href='login.css' rel='stylesheet' type='text/css'>

	<body class="no-js">
	<div id="wrapper">
		
		<h1><a href="index.php"><img src="images/logo.png" alt="La Trobe University"/></a></h1>

			
			<h2>La Trobe Admins:</h2>
					
				<form >
					<div>
						<p><label class="set-width" for="j_username">Username:</label> <input id="j_username" name="j_username" type="text"></p>
						<p><label class="set-width" for="j_password">Password:</label> <input id="j_password" name="j_password"  type="password"></p>
						<p><input value="Login" tabindex="3" type="submit"></p>
								
					</div>
				</form>
	</div>
	</body>
</html>
	